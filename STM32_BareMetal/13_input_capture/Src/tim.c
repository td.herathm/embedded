/*
 * tim.c
 *
 *  Created on: 1 Oct. 2021
 *      Author: thili
 */
#include "stm32f4xx.h"

#define	TIM2EN		(1U<<0)
#define	TIM3EN		(1U<<1)

#define CR1_CEN 	(1U<<0)
#define OC_TOGLE	((1U<<4)|(1U<<5))
#define CCER_CC1E	(1U<<0)

#define GPIOAEN		(1U<<0)
#define AFR5_TIM	(1U<<20)
#define AFR6_TIM	(1U<<25)
#define CCER_CC1S 	(1U<<0)

void tim2_1hz_init(void){
	/*enable clock access to tim2*/
	RCC->APB1ENR |= TIM2EN;

	/*set pre-scaler value*/
	TIM2->PSC = 1600 -1; // 16 000 000 / 1 600 = 10 000

	/*set auto-reload value*/
	TIM2->ARR = 10000 - 1; // 10000/10000 = 1

	/*clear counter*/
	TIM2->CNT = 0;

	/*enable timer*/
	TIM2->CR1 = CR1_CEN;
	/**/
}

void tim2_pa5_output_compare(void){
	/*enable clock access to GPIOA*/
	RCC->AHB1ENR |= GPIOAEN;

	/*set PA5 mode to alternate function*/
	GPIOA->MODER |= (1U<<11);
	GPIOA->MODER &=~(1U<<10);

	/*set PA5 alternate function type to TIM2_CH1(AF01)*/
	GPIOA->AFR[0] |= AFR5_TIM;

	/*enable clock access to tim2*/
	RCC->APB1ENR |= TIM2EN;

	/*set pre-scaler value*/
	TIM2->PSC = 1600 -1; // 16 000 000 / 1 600 = 10 000

	/*set auto-reload value*/
	TIM2->ARR = 10000 - 1; // 10000/10000 = 1

	/*set output compare toggle mode*/
	TIM2->CCMR1 = OC_TOGLE;

	/*enable tim2 ch1 in compare mode*/
	TIM2->CCER |= CCER_CC1E;

	/*clear counter*/
	TIM2->CNT = 0;

	/*enable timer*/
	TIM2->CR1 = CR1_CEN;
	/**/
}

void tim3_pa6_input_capture(void){
	/*enable clock access to GPIOA*/
	RCC->AHB1ENR |= GPIOAEN;

	/*set PA6 mode to alternate function*/
	GPIOA->MODER |= (1U<<13);
	GPIOA->MODER &=~(1U<<12);

	/*set PA6 alternate function type to TIM2_CH1(AF02)*/
	GPIOA->AFR[0] |= AFR6_TIM;

	/*enable clock access to tim3*/
	RCC->APB1ENR |= TIM3EN;

	/*set pre-scaler*/
	TIM3->PSC = 16000 -1; // 16 000 000 / 16 000

	/*set ch1 input capture*/
	TIM3->CCMR1 = CCER_CC1S;

	/*set ch1 capture at rising edge*/
	TIM3->CCER = CCER_CC1E;

	/*enable TIM3*/
	TIM3->CR1 = CR1_CEN;
	/**/
}

