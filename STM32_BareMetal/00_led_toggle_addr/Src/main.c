#define PERIPH_BASE 		(0x40000000UL) //memory map from the datasheet
#define AHB1PERIPH_OFFSET	(0x00020000UL) //GPIOA connected to AHB1 bus  from the block diagram
#define AHB1PHERIPH_BASE 	(PERIPH_BASE + AHB1PERIPH_OFFSET)
#define GPIOA_OFFSET 		(0x0000UL)

#define GPIOA_BASE			(AHB1PHERIPH_BASE + GPIOA_OFFSET)

#define RCC_OFFSET 			(0x3800UL) //datasheet block diagram
#define RCC_BASE 			(AHB1PHERIPH_BASE + RCC_OFFSET)

//enable the clock for AHB1 bus
#define AHB1ENR_R_OFFSET 	(0x30UL) // from the reference manual
#define RCC_AHB1EN_R		(*(volatile unsigned int *)(RCC_BASE + AHB1ENR_R_OFFSET))

// 2<rgisters in a GPIO, data and direction

// direction register
#define MODE_R_OFFSET 		(0x00UL) //GPIO porrt mode register from the reference manual
#define GPIOA_MODE_R		(*(volatile unsigned int *)(GPIOA_BASE + MODE_R_OFFSET))

// data register
#define OD_R_OFFSET 		(0x14UL)
#define GPIOA_OD_R 			(*(volatile unsigned int *)(GPIOA_BASE + OD_R_OFFSET))

// enable the clock for GPIOA
#define GPIOAEN 			(1U<<0)// 0b 0000 0000 0000 0000 0000 0000 0000 0001

#define PIN5				(1U<<5) // output data register pin 5
#define LED_PIN				PIN5


int main(void){
	//enable clock access to GPIOA
	RCC_AHB1EN_R |= GPIOAEN ;

	// set PA5 as output pin
	GPIOA_MODE_R |= (1U<<10);  // set bit 10 to 1
	GPIOA_MODE_R &=~ (1U<<11); // set bit 11 to 0

	while(1){
		// set led PA5 HIGH
		GPIOA_OD_R |= LED_PIN;

		/*GPIOA_OD_R ^= LED_PIN; // ON and OFF
		for(int i = 0; i<100000; i++){} */
	}

}
