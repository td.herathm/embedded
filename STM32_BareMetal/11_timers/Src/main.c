#include <stdio.h>
#include <stdint.h>
#include "stm32f4xx.h"
#include "uart.h"
#include "adc.h"
#include "systick.h"
#include "tim.h"

#define GPIOAEN		(1U<<0)
#define PIN_5		(1U<<5)
#define LED			PIN_5



int main(void){

	RCC->AHB1ENR |= GPIOAEN;
	GPIOA->MODER |= (1U<<10);
	GPIOA->MODER &=~(1U<<11);


	uart2_tx_init();
	tim2_1hz_init();

	while (1){
		/*wait for UIF*/
		while(!(TIM2->SR & SR_UIF)){}

		/*clear UIF*/
		TIM2->SR &=~ SR_UIF;

		printf("A Second passed!! \n\r");
		GPIOA->ODR ^=LED;

	}
}




