/*
 * tim.c
 *
 *  Created on: 1 Oct. 2021
 *      Author: thili
 */
#include "stm32f4xx.h"

#define	TIM2EN		(1U<<0)
#define CR1_CEN 	(1U<<0)

void tim2_1hz_init(void){
	/*enable clock access to tim2*/
	RCC->APB1ENR |= TIM2EN;

	/*set pre-scaler value*/
	TIM2->PSC = 1600 -1; // 16 000 000 / 1 600 = 10 000

	/*set auto-reload value*/
	TIM2->ARR = 10000 - 1; // 10000/10000 = 1

	/*clear counter*/
	TIM2->CNT = 0;

	/*enable timer*/
	TIM2->CR1 = CR1_CEN;
	/**/
}
