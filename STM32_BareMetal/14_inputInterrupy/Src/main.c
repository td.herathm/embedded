#include <stdio.h>
#include <stdint.h>
#include "stm32f4xx.h"
#include "uart.h"
#include "adc.h"
#include "systick.h"
#include "tim.h"
#include "exti.h"

#define GPIOAEN		(1U<<0)
#define PIN_5		(1U<<5)
#define LED			PIN_5


static void exti_callbak(void);
void EXTI15_10_IRQHandler();

int main(void){

	RCC->AHB1ENR |= GPIOAEN;
	GPIOA->MODER |= (1U<<10);
	GPIOA->MODER &=~(1U<<11);

	pc13_exti_init();
	uart2_tx_init();

	while (1){


	}
}



static void exti_callbak(void){
	printf("button pressed..\n\r");
	GPIOA->ODR ^= LED;
}


void EXTI15_10_IRQHandler(){
	if ((EXTI->PR & LINE13) != 0){
		/*clear PR flags*/
		EXTI->PR |= LINE13;
		/*do something*/
		exti_callbak();
	}
}
