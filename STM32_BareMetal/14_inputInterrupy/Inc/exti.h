/*
 * exti.h
 *
 *  Created on: 4 Oct. 2021
 *      Author: thili
 */

#ifndef EXTI_H_
#define EXTI_H_

#define	LINE13		(1U<<13)

#include "stm32f4xx.h"

void pc13_exti_init(void);


#endif /* EXTI_H_ */
