#include <stdint.h>
#define PERIPH_BASE 		(0x40000000UL) //memory map from the datasheet
#define AHB1PERIPH_OFFSET	(0x00020000UL) //GPIOA connected to AHB1 bus  from the block diagram
#define AHB1PHERIPH_BASE 	(PERIPH_BASE + AHB1PERIPH_OFFSET)
#define GPIOA_OFFSET 		(0x0000UL)

#define GPIOA_BASE			(AHB1PHERIPH_BASE + GPIOA_OFFSET)

#define RCC_OFFSET 			(0x3800UL) //datasheet block diagram
#define RCC_BASE 			(AHB1PHERIPH_BASE + RCC_OFFSET)


// enable the clock for GPIOA
#define GPIOAEN 			(1U<<0)// 0b 0000 0000 0000 0000 0000 0000 0000 0001

#define PIN5				(1U<<5) // output data register pin 5
#define LED_PIN				PIN5


typedef struct
{
	volatile uint32_t DUMMY [12];
	volatile uint32_t AHB1ENR;  /* RCC AHB1 PERIPHERAL REGISTER */

}RCC_TypeDef;

typedef struct
{
	volatile uint32_t MODER; /* GPIO PORT MODE REGISTER */
	volatile uint32_t DUMMY [4];
	volatile uint32_t ODR;  /* GPIO PORT OUTPUT DATA REGISTER */

}GPIO_TypeDef;



#define RCC ((RCC_TypeDef*) RCC_BASE)
#define GPIOA ((GPIO_TypeDef*) GPIOA_BASE )


int main(void){
	//enable clock access to GPIOA
	RCC->AHB1ENR |= GPIOAEN; //RCC_AHB1EN_R |= GPIOAEN


	// set PA5 as output pin
	GPIOA->MODER |= (1U<<10); //GPIOA_MODE_R |= (1U<<10);  // set bit 10 to 1
	GPIOA->MODER &=~ (1U<<11); //GPIOA_MODE_R &=~ (1U<<11); // set bit 11 to 0

	while(1){
		// set led PA5 HIGH
		//GPIOA_OD_R |= LED_PIN;

		GPIOA->ODR^ LED_PIN;  //GPIOA_OD_R ^= LED_PIN; // ON and OFF
		for(int i = 0; i<100000; i++){}
	}

}
