/*
 * i2c.c
 *
 *  Created on: 7 Oct. 2021
 *      Author: thili
 */
#include "stm32f4xx.h"


#define GPIOBEN 	(1U<<1)
#define I2C1EN 		(1U<<21)

#define I2C_100KHZ					80 // OB 0101 0000 = decimal 80
#define SD_MODE_MAX_RISE_TIME 		17
#define CR1_PE 		(1U<<0)

#define SR2_BUSY 	(1U<<1)
#define CR1_START	(1U<<8)
#define SR1_SB		(1U<<0)
#define SR1_ADDR	(1U<<1)
#define SR1_TXE		(1U<<7)
#define CR1_ACK		(1U<<10)
#define CR1_STOP	(1U<<9)
#define SR1_RXNE	(1U<<6)

void I2C1_init(void)
{
	/*enable clock access to GPIOB*/
	RCC->AHB1ENR |= GPIOBEN;

	/*set PB8 and PB9 mode to alternate function*/
	GPIOB->MODER &=~ (1U<<16);
	GPIOB->MODER |= (1U<<17);

	GPIOB->MODER &=~ (1U<<18);
	GPIOB->MODER |= (1U<<19);

	/*set PB8 and PB9 output type to open drain*/
	GPIOB->OTYPER |= (1U<<8);
	GPIOB->OTYPER |= (1U<<9);

	/*enable pull-up for PB8 and PB9*/
	GPIOB->PUPDR |= (1U<<16);
	GPIOB->PUPDR &=~ (1U<<17);

	GPIOB->PUPDR |= (1U<<18);
	GPIOB->PUPDR &=~ (1U<<19);

	/*enable clock access to i2c1*/
	RCC->APB1ENR |= I2C1EN;

	/*enter reset mode*/
	I2C1->CR1 |= (1U<<15);

	/*come out of reset mode*/
	I2C1->CR1 &=~ (1U<<15);

	/*set peripheral clock frequency*/
	I2C1->CR2 = (1U<<4); //16Mhz

	/*set I2C to standard mode*/
	I2C1->CCR = I2C_100KHZ;

	/*set rise time*/
	I2C1->TRISE = SD_MODE_MAX_RISE_TIME;

	/*enable I2C1 mode*/
	I2C1->CR1 |= CR1_PE;

	/**/

}

void I2C_byteRead(char saddr, char maddr, char* data)
{
	volatile int tmp;

	/*wait until bus is not busy*/
	while(I2C1->SR2 & (SR2_BUSY)){}

	/*generate start*/
	I2C1->CR1 |= CR1_START;

	/*wait until start flag is set*/
	while(!(I2C1->SR1 & (SR1_SB))){}

	/*transmit slave address + write*/
	I2C1->DR = saddr << 1;

	/*wait until addr flag is set*/
	while(!(I2C1->SR1 & (SR1_ADDR))){}

	/*clear addr flag*/
	tmp = I2C1->SR2;

	/*wait until addr flag is set*/
	while(!(I2C1->SR1 & (SR1_TXE))){}

	/*generate start*/
	I2C1->CR1 |= CR1_START;

	/*wait until start flag is set*/
	while(!(I2C1->SR1 & (SR1_SB))){}

	/*transmit slave address + read*/
	I2C1->DR = saddr << 1 | 1;

	/*wait until addr flag is set*/
	while(!(I2C1->SR1 & (SR1_ADDR))){}

	/*disable the ack*/
	I2C1->CR1 &=~  CR1_ACK;

	/*clear addr flag*/
	tmp = I2C1->SR2;

	/*generate stop after data received*/
	I2C1->CR1 |= CR1_STOP;
}


