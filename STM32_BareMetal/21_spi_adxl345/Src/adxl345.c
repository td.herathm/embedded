/*
 * adxl345.c
 *
 *  Created on: 7 Oct. 2021
 *      Author: thili
 */
#include "adxl345.h"

#define MULTY_BYTE_EN 		(0x40)
#define READ_OPRATION 		(0x80)

void adxl_read(uint8_t address, uint8_t * rxdata)
{
	/*set read operation*/
	address |= READ_OPRATION;

	/*enable multi-byte*/
	address |= MULTY_BYTE_EN;

	/*pull cs line to low to enable slave*/
	cs_enable();

	/*send address*/
	spi1_transmit(&address, 1);

	/*read 6 byte*/
	spi1_receive(rxdata, 6);

	/*pull cs line to high to disable slave*/
	cs_disable();


}

void adxl_write(uint8_t address, uint8_t value)
{
	uint8_t data[2];

	/*enable multi-byte, place address into buffer*/
	data[0] = address | MULTY_BYTE_EN ;

	/*place data into bufer*/
	data[1] = value;

	/*pull cs line to low to enable slave*/
	cs_enable();

	/*transmit data and address*/
	spi1_transmit(data, 2);

	/*pull cs line to high to disable slave*/
	cs_disable();

	/**/

}



void adxl_init(void)
{
	/*enable SPI GPIO*/
	spi_gpio_init();

	/*SPI config*/
	spi1_config();

	/*set data format range to +-4G */
	adxl_write(DATA_FORMAT_R, FOUR_G);

	/*reset all bits*/
	adxl_write(POWER_CTL_R, RESET);

	/*configure power control measure bit*/
	adxl_write(POWER_CTL_R, SET_MEASURE_B);

}

