/*
 * tim.h
 *
 *  Created on: 1 Oct. 2021
 *      Author: thili
 */

#ifndef TIM_H_
#define TIM_H_
#define SR_UIF 	(1U<<0)

void tim2_1hz_init(void);
void tim2_1hz_interrupt_init(void);

#endif /* TIM_H_ */
