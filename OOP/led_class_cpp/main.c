#include "led.h"

Led_Type led1;
Led_Type led2;


int main(void){

	USART2_Init();
	
	led_ctor(&led1, RED, OFF);
	led_get_state(&led1);
	
	led_ctor(&led2, BLUE, ON);
	led_get_state(&led2);
	led_set_state(&led1, ON);
	
	while(1){}
	
}