#include "led.h"

Led led1(RED, ON);

LedState_Type led1_state;
LedState_Type led2_state;
LedState_Type led3_state;


int main(void){

	USART2_Init();
	
	Led led2(GREEN, ON);
	Led *led3 = new Led(YELLOW, ON);
	
	
	led1_state = led1.get_state();
	led2_state = led2.get_state();
	led3_state = led3->get_state();
	
	led1.set_state(OFF);
	led2.set_state(OFF);
	led3->set_state(OFF);
	
	delete led3;
	while(1){}
	
}