#ifndef LED_H
#define LED_H
#include "stm32f4xx.h"
#include "stdint.h"
#include "uart.h"

#define LED_PORT 						GPIOB
#define LED_PORT_CLOCK			(1U<<1)

#define LED_RED_PIN 				(1U<<14)
#define LED_GREEN_PIN 			(1U<<12)
#define LED_BLUE_PIN 				(1U<<15)
#define LED_YELLOW_PIN 			(1U<<13)

#define LED_RED_MODE_BIT 				(1U<<28)
#define LED_GREEN_MODE_BIT 			(1U<<24)
#define LED_BLUE_MODE_BIT 			(1U<<30)
#define LED_YELLOW_MODE_BIT 		(1U<<26)


typedef enum {
	RED = 0,
	GREEN,
	YELLOW,
	BLUE,
}LedColors_Type;

typedef enum{
	OFF = 0,
	ON = 1,
}LedState_Type;


typedef struct{
	LedColors_Type color;
	LedState_Type state;
	//uint32_t last_update;
}Led_Type;


void led_ctor(Led_Type * const me, LedColors_Type _color, LedState_Type _state);
void led_set_state(Led_Type * const me, LedState_Type _state);
LedState_Type led_get_state(Led_Type * const me);

#endif