#include "led.h"

void led_ctor(Led_Type * const me, LedColors_Type _color, LedState_Type _state){
	
	me->color = _color;
	me->state = _state;
	
	/*hardware init*/
	RCC->AHB1ENR |= LED_PORT_CLOCK;
	
	switch (_color){
		
		case RED:
			LED_PORT->MODER |= LED_RED_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_RED_PIN;
				printf("red led is on\n\r");
			}
			else{
				LED_PORT->ODR &=~ LED_RED_PIN;
				printf("red led is off\n\r");
			}
			break;
			
		case GREEN:
			LED_PORT->MODER |= LED_GREEN_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_GREEN_PIN;
				printf("green led is on\n\r");
			}else{
				LED_PORT->ODR &=~ LED_GREEN_PIN;
				printf("green led is off\n\r");
			}
			break;
			
		case BLUE:
			LED_PORT->MODER |= LED_BLUE_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_BLUE_PIN;
				printf("blue led is on\n\r");
			}else{
				LED_PORT->ODR &=~ LED_BLUE_PIN;
				printf("blue led is off\n\r");
			}
			break;

		case YELLOW:
			LED_PORT->MODER |= LED_YELLOW_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_YELLOW_PIN;
				printf("yellow led is on\n\r");
			}else{
				LED_PORT->ODR &=~ LED_YELLOW_PIN;
				printf("yellow led is off\n\r");
			}
			break;
	}
	
}


void led_set_state(Led_Type * const me, LedState_Type _state){

	me->state = _state;
	switch (me->color){
		
		case RED:
			LED_PORT->MODER |= LED_RED_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_RED_PIN;
				printf("red led is on\n\r");
			}
			else{
				LED_PORT->ODR &=~ LED_RED_PIN;
				printf("red led is off\n\r");
			}
			break;
			
		case GREEN:
			LED_PORT->MODER |= LED_GREEN_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_GREEN_PIN;
				printf("green led is on\n\r");
			}else{
				LED_PORT->ODR &=~ LED_GREEN_PIN;
				printf("green led is off\n\r");
			}
			break;
			
		case BLUE:
			LED_PORT->MODER |= LED_BLUE_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_BLUE_PIN;
				printf("blue led is on\n\r");
			}else{
				LED_PORT->ODR &=~ LED_BLUE_PIN;
				printf("blue led is off\n\r");
			}
			break;

		case YELLOW:
			LED_PORT->MODER |= LED_YELLOW_MODE_BIT;
			if(me->state == ON){
				LED_PORT->ODR |= LED_YELLOW_PIN;
				printf("yellow led is on\n\r");
			}else{
				LED_PORT->ODR &=~ LED_YELLOW_PIN;
				printf("yellow led is off\n\r");
			}
			break;
	}

}


LedState_Type led_get_state(Led_Type * const me){

	switch(me->color){
		case RED:
			printf("RED led is currently %d\n\r", me->state);
			break;
		
		case GREEN:
			printf("GREEN led is currently %d\n\r", me->state);
			break;
		
		case BLUE:
			printf("BLUE led is currently %d\n\r", me->state);
			break;
		
		case YELLOW:
			printf("YELLOW led is currently %d\n\r", me->state);
			break;
	}	
	
	return me->state;

}