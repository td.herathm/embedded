#ifndef __UART_H
#define __UART_H

#include "stm32f4xx.h"                  // Device header
#include "stdio.h"

void USART2_Init(void);
uint32_t USART2_write(uint32_t ch);
uint32_t USART2_read(void);
void test_setup(void);


#endif 

